KCONFIG_EXT_PREFIX=${DEVICE_MODULES_DIR}
# TODO(b/231473697): use realpath --relative-to
# rel_path does not work with Kleaf --config=local builds because
# rel_path is not simplified enough (e.g. the following evaluates to
# ../../__main__ in Kleaf, which does not work in non-sandbox builds).
# Hence use rel_path2 so it evaluates to a simpler path ../
KCONFIG_EXT_MODULES_PREFIX=$(rel_path2 ${ROOT_DIR} ${KERNEL_DIR})/

. ${ROOT_DIR}/${KERNEL_DIR}/build.config.common
. ${ROOT_DIR}/${KERNEL_DIR}/build.config.aarch64

DEFCONFIG=sw5100_gki_defconfig
MERGE_CONFIG_EXTRA_ARGS=(
  ${ROOT_DIR}/${KERNEL_DIR}/../google-modules/soc/msm/monaco_gki.fragment
  ${ROOT_DIR}/${KERNEL_DIR}/../google-modules/soc/msm/sw5100.fragment
)
PRE_DEFCONFIG_CMDS="KCONFIG_CONFIG=${ROOT_DIR}/${KERNEL_DIR}/arch/arm64/configs/${DEFCONFIG} \
${ROOT_DIR}/${KERNEL_DIR}/scripts/kconfig/merge_config.sh -m -r \
${ROOT_DIR}/${KERNEL_DIR}/arch/arm64/configs/gki_defconfig ${MERGE_CONFIG_EXTRA_ARGS[@]} \
${GKI_FRAGMENT_DEFCONFIG}"

POST_DEFCONFIG_CMDS="rm ${ROOT_DIR}/${KERNEL_DIR}/arch/arm64/configs/${DEFCONFIG}"

MAKE_GOALS=""
# Keep in sync with KERNEL_BOOT_IMG in download_gki.sh
KERNEL_BINARY=Image

if [[ "${BUILD_KERNEL}" == "1" ]]; then
  BUILD_BOOT_IMG=1
fi

BUILD_INITRAMFS=1
BUILD_VENDOR_KERNEL_BOOT=1
PAGE_SIZE=4096
LZ4_RAMDISK=1
BOOT_IMAGE_HEADER_VERSION=4
TRIM_UNUSED_MODULES=1
# Required for devicetree overlay support.
DTC_FLAGS="-@"
DTC_INCLUDE=${ROOT_DIR}/private/msm-google-modules/audio/include
MODULES_LIST=${DEVICE_MODULES_DIR}/vendor_kernel_boot_modules.sw5100
VENDOR_DLKM_MODULES_LIST=${DEVICE_MODULES_DIR}/vendor_dlkm_modules.sw5100
VENDOR_DLKM_MODULES_BLOCKLIST=${DEVICE_MODULES_DIR}/vendor_dlkm.blocklist.sw5100
VENDOR_DLKM_PROPS=${DEVICE_MODULES_DIR}/vendor_dlkm.props.sw5100

FILES="
google-dts-base/sw5100.dtb
google-dts-base/sw5100-btwifi.dtb
"

EXT_MODULES="
private/msm-google-modules/audio
private/msm-google-modules/bt
private/msm-google-modules/dataipa/drivers/platform/msm
private/msm-google-modules/datarmnet/core
private/msm-google-modules/datarmnet-ext/wlan
private/msm-google-modules/display
private/msm-google-modules/graphics
private/msm-google-modules/mm/hw_fence
private/msm-google-modules/mm/msm_ext_display
private/msm-google-modules/mm/sync_fence
private/msm-google-modules/mmrm
private/msm-google-modules/securemsm
private/msm-google-modules/video
private/msm-google-modules/wlan/platform
private/msm-google-modules/wlan/qcacld-3.0
"

if false; then
EXT_MODULES+="
private/msm-google-modules/data-kernel/drivers/emac-dwc-eqos/
"
fi

WLAN_PROFILE=wear

# Prevent GKI kernel build from overwriting our kernel-uapi-headers.tar.gz file
GKI_SKIP_CP_KERNEL_HDR=1

if [[ -n "${GKI_BUILD_CONFIG_FRAGMENT}" ]]; then
  . ${GKI_BUILD_CONFIG_FRAGMENT}
fi

if [[ -z "${FAST_BUILD}" ]]; then
  COMPRESS_UNSTRIPPED_MODULES=1
  UNSTRIPPED_MODULES="
  *.ko
  "
fi

INSTALL_MOD_STRIP=1
