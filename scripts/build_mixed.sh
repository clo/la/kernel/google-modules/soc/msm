#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
#
# Generate GKI kernel and device kernel binaries

# Need to be exported for build.sh usage.
export LTO=${LTO:-thin}
export BUILD_CONFIG
export GKI_BUILD_CONFIG
export GKI_PREBUILTS_DIR

BUILD_KERNEL=${BUILD_KERNEL:-1}

function exit_if_error() {
  if [[ $1 -ne 0 ]]; then
    echo "ERROR: $2: retval=$1" >&2
    exit $1
  fi
}

function check_dirty_common() {
  if [[ -f ${GKI_PREBUILTS_DIR}/vmlinux ]]; then
    local sha_file=vmlinux
  else
    local sha_file=boot.img
  fi

  local prebuilts_sha=$(strings ${GKI_PREBUILTS_DIR}/${sha_file} |
                        grep "Linux version 5.15" | sed -n "s/^.*-g\([0-9a-f]\{12\}\)-.*/\1/p")
  local common_sha=$(git -C common/ log -1 --abbrev=12 --pretty="format:%h")
  local modified_kernel_files=$(git -C common/ --no-optional-locks status -uno --porcelain ||
                                git -C common/ diff-index --name-only HEAD)

  if [[ "${prebuilts_sha}" != "${common_sha}" ]] || [[ -n "${modified_kernel_files}" ]]; then
    echo "WARNING: There are changes in common/ which are not in the prebuilts."
    echo "Because you did not specify BUILD_KERNEL=1, $0 defaulted to building"
    echo "with the prebuilts. Please be aware that your changes to common/ will not"
    echo "be present in the final images. If you have made changes to common/"
    echo "that you wish to build, it is recommended you explicitly set BUILD_KERNEL=1."
    echo "Otherwise, the prebuilts will be used."
  fi
}

if [[ -z "${DEVICE_KERNEL_BUILD_CONFIG}" ]]; then
  echo "DEVICE_KERNEL_BUILD_CONFIG must be specified to create a mixed build." >&2
  exit 1
else
  BUILD_CONFIG="${DEVICE_KERNEL_BUILD_CONFIG}"
fi

if [[ -n "${GKI_BUILD_CONFIG_FRAGMENT}" ]]; then
  BUILD_KERNEL=1
fi

if [[ "${BUILD_KERNEL}" = 1 ]]; then
  GKI_BUILD_CONFIG="common/build.config.gki.aarch64"
else
  GKI_PREBUILTS_DIR="$(readlink -m "prebuilts/boot-artifacts/kernel")"
fi

build/build.sh "$@"

exit_if_error $? "Failed to create mixed build"

if [[ "${BUILD_KERNEL}" != 1 ]]; then
  check_dirty_common
fi
