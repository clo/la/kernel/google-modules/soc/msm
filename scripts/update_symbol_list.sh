#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
#
# Update the general symbol list and prepare a suitable commit
# message for AOSP submission.

GKI_KERNEL_DIR="common"
GKI_KERNEL_REMOTE="aosp"
GKI_KERNEL_BRANCH=$(. ${GKI_KERNEL_DIR}/build.config.constants && echo ${BRANCH})
DEVICE_KERNEL_DIR="private/msm-google"
SYMBOL_LIST="android/abi_gki_aarch64_general"
OUT_DIR="out/${GKI_KERNEL_BRANCH}"
DIST_DIR=${OUT_DIR}/dist
VMLINUX_TMP=${OUT_DIR}/private/vmlinux
GKI_MODULES_LIST="android/gki_aarch64_modules"
PREPARE_AOSP_SYMLIST="0"
BUG=""
COMMIT_TEXT=""
ARGS=()

function usage() {
  cat <<- EOF
    USAGE: $0 [-p|--prepare-aosp-symlist BUG_NUMBER] [-d|--device DEVICE_NAME]
    -p | --prepare-aosp-symlist BUG_NUMBER   Update the AOSP symbol list in ${GKI_KERNEL_DIR}/
                                             and create a commit with the provided BUG_NUMBER.
    -d | --device DEVICE_NAME                Device to build kernel modules for (e.g. eos, p11).
EOF
}

# Add a trap to remove the temporary vmlinux in case of an error occurs before
# we finish.
function cleanup_trap() {
  rm -f ${VMLINUX_TMP} ${COMMIT_TEXT}
  exit $1
}
trap 'cleanup_trap' EXIT

function exit_if_error() {
  if [[ $1 -ne 0 ]]; then
    echo "ERROR: $2: retval=$1" >&2
    exit $1
  fi
}

function verify_aosp_tree {
  if [[ "${PREPARE_AOSP_SYMLIST}" = "0" ]]; then
    return
  fi

  pushd ${GKI_KERNEL_DIR} > /dev/null

  if ! git diff --quiet HEAD; then
    exit_if_error 1 \
    "Found uncommitted changes in ${GKI_KERNEL_DIR}/. Commit your changes before updating the symbol list"
  fi

  popd > /dev/null
}

function make_symbol_list_commit_text() {
  COMMIT_TEXT=$(mktemp -t symlist_commit_text.XXXXX)
  echo "ANDROID: GKI: Update the ABI symbol list" > ${COMMIT_TEXT}
  echo >> ${COMMIT_TEXT}
  echo "Update the general symbol list." >> ${COMMIT_TEXT}
  echo >> ${COMMIT_TEXT}
  echo "Bug: ${BUG}" >> ${COMMIT_TEXT}
}

function commit_symbol_list_update() {
  make_symbol_list_commit_text

  pushd ${GKI_KERNEL_DIR} > /dev/null

  if ! git cat-file -e \
          ${GKI_KERNEL_REMOTE}/${GKI_KERNEL_BRANCH}:${SYMBOL_LIST} \
          > /dev/null 2>&1; then
    git add ${SYMBOL_LIST}
  fi

  git commit --quiet -s -F ${COMMIT_TEXT} -- android/
  rm -f ${COMMIT_TEXT}

  cat <<- EOF

    An ABI commit in ${GKI_KERNEL_DIR}/ was created for you.
    Please verify your commit(s) before pushing. Here are the steps to perform:

    cd ${GKI_KERNEL_DIR}
    git log --oneline HEAD
    git push ${GKI_KERNEL_REMOTE} HEAD:refs/for/${GKI_KERNEL_BRANCH}

    After your commit has been uploaded, please visit the Gerrit link that
    corresponds to your commit, and use the "Rebase" button to rebase to the
    tip of the ${GKI_KERNEL_BRANCH} branch.
EOF

  popd > /dev/null
}

function update_aosp_symlist() {
  extract_device_symbols 1 "${GKI_KERNEL_DIR}/${SYMBOL_LIST}"
  echo "========================================================"
  echo " The symbol list has been updated locally in ${GKI_KERNEL_DIR}/."
  echo " Compiling with BUILD_KERNEL=1 is now required until"
  echo " the new symbol(s) are merged."
}

# Extract the kernel module symbols.
#
# $1 Specifies if --additions-only should be used
# $2 The symbol list to update/create
function extract_device_symbols {
  local clang_version=$(. ${DEVICE_KERNEL_DIR}/build.config.constants && \
    echo $CLANG_VERSION)
  local clang_prebuilt_bin=prebuilts/clang/host/linux-x86/clang-${clang_version}/bin
  local device_kernel_out_dir=${OUT_DIR}/private/
  local additions_only=$1
  local symbol_list=$2

  if [[ "${additions_only}" != "0" ]]; then
    ADD_ONLY_FLAG="--additions-only"
  fi

  echo "========================================================"
  echo " Extracting symbols and updating the symbol list"

  # Need to copy over the vmlinux to be under the same directory as we point
  # extract_symbols to.
  cp ${DIST_DIR}/vmlinux ${VMLINUX_TMP}

  if ! [[ -f "${symbol_list}" ]]; then
    touch ${symbol_list}
  fi

  PATH=${PATH}:${clang_prebuilt_bin}
  build/abi/extract_symbols              \
      --symbol-list ${symbol_list} \
      --skip-module-grouping             \
      ${ADD_ONLY_FLAG}                   \
      --gki-modules ${DIST_DIR}/$(basename ${GKI_MODULES_LIST}) \
      ${device_kernel_out_dir}
  exit_if_error $? "Failed to extract symbols!"

  rm -f ${VMLINUX_TMP}
}

# Build the GKI kernel and device kernel modules with symbol trimming and KMI
# enforcement disabled. This ensures that the binaries can be built without
# newly exported symbols being present in a symbol list.
function build_kernel_binaries() {
  SKIP_MRPROPER=1 \
  BUILD_KERNEL=1 \
  TRIM_NONLISTED_KMI=0 \
  KMI_SYMBOL_LIST_STRICT_MODE=0 \
  DEVICE_KERNEL_BUILD_CONFIG="${DEVICE_KERNEL_BUILD_CONFIG}" \
  private/google-modules/soc/msm/scripts/build_mixed.sh "$@"
  exit_if_error $? "Failed to create mixed build"
}

function read_args() {
  local next
  local device_name

  while [[ $# -gt 0 ]]; do
    next="$1"

    case ${next} in
      -p|--prepare-aosp-symlist)
        PREPARE_AOSP_SYMLIST="1"
        BUG="$2"
        if ! [[ "${BUG}" =~ ^[0-9]+$ ]]; then
          exit_if_error 1 "Bug numbers should be digits."
        fi
        shift
        ;;
      -d|--device)
        device_name="$2"
        DEVICE_KERNEL_BUILD_CONFIG="private/devices/google/${device_name}/build.config.${device_name}"
        if ! [[ -f "${DEVICE_KERNEL_BUILD_CONFIG}" ]]; then
          exit_if_error 1 "${device_name} not supported; ${DEVICE_KERNEL_BUILD_CONFIG} not found"
        fi
        shift
        ;;
      -h|--help)
        usage
        exit 0
        ;;
      *)
        ARGS+=("$1")
        ;;
    esac
    shift
  done

  if [[ -z "${DEVICE_KERNEL_BUILD_CONFIG}" ]]; then
    echo "ERROR: --device is a required argument" >&2
    usage
    exit 1
  fi
}

read_args "$@"

# read_args() will ensure to store any arguments that should be passed on
# to the build scripts into the $ARGS array. However, this must be done
# outside of read_args() to affect the positional arguments for the invocation
# of this script, and not just the invocation of read_args().
set -- "${ARGS[@]}"

# Verify the aosp tree is in a good state before compiling anything
verify_aosp_tree

build_kernel_binaries "$@"

update_aosp_symlist

if [[ "${PREPARE_AOSP_SYMLIST}" != "0" ]]; then
  commit_symbol_list_update
fi
