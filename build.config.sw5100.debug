# Disable symbol trimming and ABI monitoring for debug kernels.
TRIM_NONLISTED_KMI=0
KMI_SYMBOL_LIST_STRICT_MODE=0

# MODULES_LIST and MODULES_ORDER are set by build.config.gki.aarch64 to ensure
# that only the modules declared in android/gki_aarch64_modules are part of the
# system_dlkm partition. This is not necessary for debug kernels, so override
# MODULES_LIST and MODULES_ORDER to allow all modules that are compiled with a
# GKI-based debug kernel to be part of the system_dlkm partition.
GKI_MODULES_LIST=
MODULES_LIST=
MODULES_ORDER=
SYSTEM_DLKM_MODULES_BLOCKLIST=

# Config options taken from private/msm-google/arch/arm64/configs/consolidate.fragment.
function update_gki_debug_config() {
  ${KERNEL_DIR}/scripts/config --file ${OUT_DIR}/.config \
    -e CONFIG_BOOTPARAM_SOFTLOCKUP_PANIC \
    --set-val CONFIG_BOOTPARAM_SOFTLOCKUP_PANIC_VALUE 1 \
    --set-str CONFIG_CMDLINE "kasan.stacktrace=off stack_depot_disable=off page_owner=on no_hash_pointers panic_on_taint=0x20" \
    -e CONFIG_DEBUG_KMEMLEAK \
    --set-val CONFIG_DEBUG_KMEMLEAK_MEM_POOL_SIZE 16000 \
    -e CONFIG_DEBUG_MUTEXES \
    -e CONFIG_DEBUG_OBJECTS \
    --set-val CONFIG_DEBUG_OBJECTS_ENABLE_DEFAULT 1 \
    -e CONFIG_DEBUG_OBJECTS_TIMES \
    -e CONFIG_DEBUG_OBJECTS_WORK \
    -e CONFIG_DEBUG_PREEMPT \
    -e CONFIG_DEBUG_RWSEMS \
    -e CONFIG_DEBUG_SPINLOCK \
    -e CONFIG_DYNAMIC_DEBUG \
    -e CONFIG_LOCKUP_DETECTOR \
    -e CONFIG_PID_IN_CONTEXTIDR \
    -e CONFIG_PM_DEBUG \
    -e CONFIG_PM_SLEEP_DEBUG \
    -e CONFIG_RUNTIME_TESTING_MENU \
    -e CONFIG_SERIAL_QCOM_GENI_CONSOLE_DEFAULT_ENABLED \
    -e CONFIG_SOFTLOCKUP_DETECTOR \
    -e CONFIG_SW_SYNC \
    -m CONFIG_ATOMIC64_SELFTEST \
    -e CONFIG_CMA_DEBUGFS \
    -e CONFIG_DEBUG_PAGEALLOC \
    -e CONFIG_DEBUG_PAGEALLOC_ENABLE_DEFAULT \
    -e CONFIG_DETECT_HUNG_TASK \
    -m CONFIG_LKDTM \
    -m CONFIG_LOCK_TORTURE_TEST \
    -m CONFIG_RCU_TORTURE_TEST \
    -m CONFIG_TEST_USER_COPY
  (cd ${OUT_DIR} && \
   make O=${OUT_DIR} "${TOOL_ARGS[@]}" ${MAKE_ARGS} olddefconfig)
}

# Config options taken from private/msm-google/arch/arm64/configs/vendor/monaco_consolidate.config.
function update_device_modules_debug_config() {
  ${KERNEL_DIR}/scripts/config --file ${OUT_DIR}/.config \
    -e CONFIG_CNSS2_DEBUG \
    -m CONFIG_CORESIGHT_SOURCE_ETM4X \
    -e CONFIG_BUG_ON_HW_MEM_ONLINE_FAIL \
    -e CONFIG_HYP_ASSIGN_DEBUG \
    -m CONFIG_QCOM_RTB \
    -e CONFIG_QCOM_RTB_SEPARATE_CPUS \
    -e CONFIG_QTI_PMIC_GLINK_CLIENT_DEBUG
  (cd ${OUT_DIR} && \
   make O=${OUT_DIR} "${TOOL_ARGS[@]}" ${MAKE_ARGS} olddefconfig)
}

append_cmd POST_DEFCONFIG_CMDS update_gki_debug_config

if [[ -n "${GKI_BUILD_CONFIG}" ]]; then
  append_cmd POST_DEFCONFIG_CMDS update_device_modules_debug_config
fi
