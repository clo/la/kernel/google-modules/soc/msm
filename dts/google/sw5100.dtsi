// SPDX-License-Identifier: GPL-2.0
// base on: monaco.dtsi

#include "../vendor/qcom/monaco.dtsi"
#include "../audio-devicetree/monaco-audio.dtsi"
#include "../audio-devicetree/monaco-audio-overlay.dtsi"
#include "../video-devicetree/monaco-vidc.dtsi"
#include "../graphics-devicetree/gpu/monaco-gpu.dtsi"
#include "../wlan-devicetree/monaco-standalone-cnss.dtsi"
#include "../display-devicetree/display/monaco-sde-display.dtsi"
#include "sw5100-i2s.dtsi"
#include "sw5100-pinctrl.dtsi"
#include "sw5100-qupv3.dtsi"
#include "sw5100-sdhc.dtsi"

&firmware {
	/delete-node/ android;
};

&tx_macro {
	/delete-property/ qcom,is-used-swr-gpio;
	qcom,swr-gpio-is-used = <0>;
};

&swr1 {
	interrupt-parent = <&intc>;
	interrupts = <GIC_SPI 297 IRQ_TYPE_LEVEL_HIGH>;
};

&va_macro {
	/delete-property/ qcom,is-used-swr-gpio;
	qcom,swr-gpio-is-used = <1>;
};

&swr0 {
	interrupt-parent = <&intc>;
	interrupts =
		<GIC_SPI 296 IRQ_TYPE_LEVEL_HIGH>,
		<GIC_SPI 79 IRQ_TYPE_LEVEL_HIGH>;
};

&soc {
	/delete-node/ qcom,battery-data;

	sw5100_batterydata: qcom,battery-data {
		qcom,batt-id-range-pct = <15>;
	};

	subsystem-sleep-stats@4690000 {
		compatible = "qcom,subsystem-sleep-stats-v2";
		reg = <0x4690000 0x400>;
		ddr-freq-update;
	};

};

&pm5100_charger {
	qcom,battery-data = <&sw5100_batterydata>;
};

&monaco_snd {
	/delete-property/ qcom,cdc-dmic01-gpios;
	/delete-property/ qcom,cdc-dmic23-gpios;

	qcom,mi2s-audio-intf = <0>;
};
